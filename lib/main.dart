import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute:
          'pilulas', // Defini a rota em que o aplicativo vai começar primeiro
      routes: {
        // por meio das routes definimos as paginas das rotas
        'pilulas': (context) =>
            Pilulas(), // cada umas linhas é uma rota para uma pagina
        'pilulaAzul': (context) => PilulaAzul(),
        'pilulaVermelha': (context) => PilulaVermelha(),
      },
    );
  }
}

class Pilulas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center, // Deixa centralizado
        children: <Widget>[
          Image.network(
              'https://i.pinimg.com/originals/ed/89/2a/ed892a0bfc4f18c263768d545df38f01.jpg'),
          // filhos permite ter varios filhos
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Qual pilula que você vai escolher?',
              style: TextStyle(fontSize: 20),
            ),
          ),
          SizedBox(
            width: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center, // Deixa centralizado
            children: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: Colors.red),
                onPressed: () {
                  Navigator.pushNamed(context,
                      'pilulaVermelha'); // criando rota para a pilular vermelha
                },
                child: Text('Vermelha'),
              ),
              SizedBox(
                width: 25,
              ),
              SizedBox(
                width: 100,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.blue),
                  onPressed: () {
                    Navigator.pushNamed(context, 'pilulaAzul');
                  },
                  child: Text('Azul'),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class PilulaAzul extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: (Column(
          mainAxisAlignment: MainAxisAlignment.center, // deia centralizado
          children: <Widget>[
            SizedBox(
              height: 25,
            ),
            Image.network('https://pbs.twimg.com/media/Eraws82XUAEzxMR.jpg'),
            SizedBox(
              height: 25,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Voltar'))
          ],
        )),
      ),
    );
  }
}

class PilulaVermelha extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: (Column(
          mainAxisAlignment: MainAxisAlignment.center, // deia centralizado
          children: <Widget>[
            SizedBox(
              height: 25,
            ),
            Image.network('https://pbs.twimg.com/media/Erawst7WMAAn3aq.jpg'),
            SizedBox(
              height: 25,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Voltar'))
          ],
        )),
      ),
    );
  }
}
